<?php

namespace App\Models;


class AppointmentService extends BaseModel
{
    protected $table = 'appointment_service';

    protected $fillable = [
        'appointment_id', 'service_id'
    ];
}
