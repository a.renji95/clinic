<?php

namespace App\Models;

class Diagnostic extends BaseModel
{
    protected $table = 'diagnostic';

    protected $fillable = [
        'description', 'appointment_id'
    ];
}
