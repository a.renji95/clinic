<?php

namespace App\Models;

class Invoice extends BaseModel
{
    protected $table = 'invoice';

    protected $fillable = [
        'status_id', 'patient_id', 'appointment_id', 'amount', 'payment_date',
    ];
}
