<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Service::truncate();
        Schema::enableForeignKeyConstraints();

        Service::create([
            'name' => 'Khám Sản',
            'price' => 500000,
        ]);

        Service::create([
            'name' => 'Khám Nhi',
            'price' => 100000,
        ]);

        Service::create([
            'name' => 'Khám Nội tổng quát',
            'price' => 1000000,
        ]);

        Service::create([
            'name' => 'Khám Tai Mũi Họng',
            'price' => 100000,
        ]);

        Service::create([
            'name' => 'Khám Răng Hàm Mặt',
            'price' => 100000,
        ]);

        Service::create([
            'name' => 'Chẩn đoán hình ảnh',
            'price' => 150000,
        ]);

        Service::create([
            'name' => 'Xét nghiệm',
            'price' => 200000,
        ]);

        Service::create([
            'name' => 'Chụp X-Quang',
            'price' => 150000,
        ]);
    }
}
