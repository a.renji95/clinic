<?php

namespace App\Models;

class Shift extends BaseModel
{
    protected $table = 'shift';

    protected $fillable = [
        'startTime', 'endTime'
    ];
}
